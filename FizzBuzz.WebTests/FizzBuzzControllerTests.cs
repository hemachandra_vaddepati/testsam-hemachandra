﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FizzBuzz.Service;
using FizzBuzz.Service.Handlers;
using NUnit.Framework;
using TestSam.Controllers;

namespace FizzBuzz.WebTests
{
    [TestFixture()]
    public class FizzBuzzControllerTests
    {
        private readonly IDayOfWeekHandler _wednesdayHandler = new WednesdayHandler();
        [Test]
        public void OnWebSiteDefaultLoad_Index_ReturnsIndexView()
        {
            List<INumeralHandler> numeralHandlers = new List<INumeralHandler>
            {
                new DivisibleByThreeNumeralHandler(_wednesdayHandler),
                new DivisibleByFiveNumeralHandler(_wednesdayHandler),
                new DivisibleByThreeAndFiveNumeralHandler(_wednesdayHandler)
            };


            var controller = new FizzBuzzController(new FizzBuzzService(numeralHandlers.AsEnumerable()));
            var result = controller.Index() as ViewResult;
            if (result != null) Assert.That(result.ViewName, Is.EqualTo("Index"));
        }
    }
}
