﻿using System.Collections.Generic;
using System.Linq;
using FizzBuzz.Service.Handlers;

namespace FizzBuzz.Service
{
    /// <summary>
    /// Class holds custom 'fizz-buzz' logic to generate list of words
    /// </summary>
    public class FizzBuzzService : IFizzBuzzService
    {
        private readonly IEnumerable<INumeralHandler> _numeralHandlers;

        public FizzBuzzService(IEnumerable<INumeralHandler> numeralHandlers)
        {
            _numeralHandlers = numeralHandlers;
        }

        /// <summary>
        /// Returns list of words
        /// </summary>
        /// <param name="userEnteredValue">Higher number prompted by user</param>
        /// <returns>List of numbers in string</returns>
        public List<string> GetFizzBuzzData(int userEnteredValue)
        {      
            var lstValues = new List<string>();

            for (var i = 1; i <= userEnteredValue; i++)
            {
                var matchedNumeralHandler =  _numeralHandlers.SingleOrDefault(x => x.CanHandle(i));
                if (matchedNumeralHandler != null)
                {
                    string value = matchedNumeralHandler.GetFizzBuzzWord();
                    lstValues.Add(value);
                }
                else
                {
                    lstValues.Add(i.ToString());
                }
            }
            return lstValues;
        }
    }
}