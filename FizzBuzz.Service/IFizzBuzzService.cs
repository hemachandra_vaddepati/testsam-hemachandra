﻿using System.Collections.Generic;

namespace FizzBuzz.Service
{
    /// <summary>
    /// Interface representing classes, which have logic for generating list of words.
    /// </summary>
    public interface IFizzBuzzService
    {
       List<string> GetFizzBuzzData(int userEnteredValue);
    }
}
