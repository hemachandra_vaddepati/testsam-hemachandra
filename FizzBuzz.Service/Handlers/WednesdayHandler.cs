﻿using System;
using System.Text;

namespace FizzBuzz.Service.Handlers
{
    public class WednesdayHandler : IDayOfWeekHandler
    {
        public bool CanHandle(DayOfWeek dayOfWeek)
        {
            return dayOfWeek == DayOfWeek.Wednesday;
        }

        public string GetNewValue(string value)
        {
            var oldValue = new StringBuilder(value);
            return oldValue.Replace("fizz", "wizz").Replace("buzz", "wuzz").ToString();
        }
    }
}
