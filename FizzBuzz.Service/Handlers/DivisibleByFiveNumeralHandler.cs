﻿using System;

namespace FizzBuzz.Service.Handlers
{
    /// <summary>
    /// Class holds numeral which produces buzz as word.
    /// </summary>
    public class DivisibleByFiveNumeralHandler : INumeralHandler
    {
        private readonly IDayOfWeekHandler _dayOfWeekHandler;
        public DivisibleByFiveNumeralHandler(IDayOfWeekHandler dayOfWeekHandler)
        {
            _dayOfWeekHandler = dayOfWeekHandler;
        }

        public bool CanHandle(int number)
        {
            return number % 3 != 0 && number % 5 == 0;
        }

        public string GetFizzBuzzWord()
        {
            string value = "buzz";

            if (_dayOfWeekHandler != null && _dayOfWeekHandler.CanHandle(DateTime.Today.DayOfWeek))
            {
                value = _dayOfWeekHandler.GetNewValue(value);
            }

            return value;
        }
    }
}