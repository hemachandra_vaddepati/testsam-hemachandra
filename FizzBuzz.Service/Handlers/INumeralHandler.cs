﻿namespace FizzBuzz.Service.Handlers
{
    /// <summary>
    /// abstract class Numeral represents all numbers
    /// Demonstrated Open/Closed Principle and Chain-of-responsibility pattern
    /// </summary>
    public interface INumeralHandler
    {
        /// <summary>
        ///check whether it will handle request else pass to next handler
        /// </summary>
        bool CanHandle(int number);

        /// <summary>
        /// Gets custom word for handled number
        /// </summary>
        /// <returns></returns>
        string GetFizzBuzzWord();
    }
}