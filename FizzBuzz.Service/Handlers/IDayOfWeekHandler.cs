﻿using System;

namespace FizzBuzz.Service.Handlers
{
    public interface IDayOfWeekHandler
    {
        bool CanHandle(DayOfWeek dayOfWeek);

        string GetNewValue(string value);
    }
}
