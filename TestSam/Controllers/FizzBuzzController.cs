﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using FizzBuzz.Service;
using FizzBuzz.Web.Models;
using PagedList;

namespace TestSam.Controllers
{
    public class FizzBuzzController : Controller
    {
        private int _pageSize = 20;
        private readonly IFizzBuzzService _fizzBuzzService;

        public FizzBuzzController(IFizzBuzzService fizzBuzzService)
        {
            _fizzBuzzService = fizzBuzzService;
        }

        public ActionResult Index()
        {
            return View("Index");
        }

        /// <summary>
        /// Method to get list of numbers in words
        /// </summary>
        /// <param name="txtMaxNumber">user entered max value</param>
        /// <param name="page"></param>
        /// <returns></returns>
        
        public ActionResult GetData(string UserInput, int? page)
        {
            int pageIndex = 1;
            ViewBag.UserInput = Convert.ToInt32(UserInput);
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            List<string> lstWords = _fizzBuzzService.GetFizzBuzzData(Convert.ToInt32(UserInput));

            return View("Index", new FizzBuzzData { FizzBuzzList = lstWords.ToPagedList(pageIndex, _pageSize), UserInput = ViewBag.UserInput});
        }

 
      
    }
}