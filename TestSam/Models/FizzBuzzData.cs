﻿using System.ComponentModel.DataAnnotations;

namespace FizzBuzz.Web.Models
{
    public class FizzBuzzData
    {
        [Required(ErrorMessage = "*Mandatory")]
        [Range(1, 1000, ErrorMessage = "Invalid number")]
        
        public int UserInput { get; set; }

        public PagedList.IPagedList<string> FizzBuzzList { get; set; }
    }
}