﻿using System;
using FizzBuzz.Service.Handlers;
using NUnit.Framework;

namespace FizzBuzz.ServiceTests.Handlers
{
    [TestFixture]
    class DivisibleByThreeNumeralHandlerTests
    {
        private readonly IDayOfWeekHandler _wednesdayHandler = new WednesdayHandler();
        [Test]
        public void ShouldHandleNumbersDivisibleByThree_CanHandle_ReturnTrue()
        {
            INumeralHandler divisibleByThree = new DivisibleByThreeNumeralHandler(_wednesdayHandler);
            Assert.AreEqual(true, divisibleByThree.CanHandle(9));
        }

        [Test]
        public void ShouldNotHandleNumbersNotDivisibleByThree_CanHandle_ReturnFalse()
        {
            INumeralHandler divisibleByThree = new DivisibleByThreeNumeralHandler(_wednesdayHandler);
            Assert.AreEqual(false, divisibleByThree.CanHandle(7));
        }

        [Test]
        public void IfWednesdayGetFizzElseWizz_GetNumberInStringFormat_ReturnsString()
        {
            INumeralHandler divisibleByThree = new DivisibleByThreeNumeralHandler(_wednesdayHandler);
            Assert.AreEqual(DateTime.Today.DayOfWeek == DayOfWeek.Wednesday ? "wizz" : "fizz",
                divisibleByThree.GetFizzBuzzWord());
        }


    }

}
