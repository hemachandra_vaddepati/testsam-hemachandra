﻿using System;
using FizzBuzz.Service.Handlers;
using NUnit.Framework;

namespace FizzBuzz.ServiceTests.Handlers
{
    [TestFixture]
    class DivisibleByFiveNumeralHandlerTests
    {
        private readonly IDayOfWeekHandler _wednesdayHandler = new WednesdayHandler();
        [Test]
        public void ShouldHandleNumbersDivisibleByFive_CanHandle_ReturnTrue()
        {
            INumeralHandler divisibleByFive = new DivisibleByFiveNumeralHandler(_wednesdayHandler);
            Assert.AreEqual(true, divisibleByFive.CanHandle(10));
        }

        [Test]
        public void ShouldNotHandleNumbersNotDivisibleByFive_CanHandle_ReturnFalse()
        {
            INumeralHandler divisibleByFive = new DivisibleByFiveNumeralHandler(_wednesdayHandler);
            Assert.AreEqual(false, divisibleByFive.CanHandle(7));
        }

        [Test]
        public void IfWednesdayGetBuzzElseWuzz_GetNumberInStringFormat_ReturnsString()
        {
            INumeralHandler divisibleByFive = new DivisibleByFiveNumeralHandler(_wednesdayHandler);
            Assert.AreEqual(DateTime.Today.DayOfWeek == DayOfWeek.Wednesday ? "wuzz" : "buzz",
                divisibleByFive.GetFizzBuzzWord());
        }

    }

}
