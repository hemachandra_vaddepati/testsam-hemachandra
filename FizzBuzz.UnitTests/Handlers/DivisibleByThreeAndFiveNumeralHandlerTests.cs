﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzBuzz.Service.Handlers;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace FizzBuzz.ServiceTests.Handlers
{
    [TestFixture()]
    class DivisibleByThreeAndFiveNumeralHandlerTests
    {
        private readonly IDayOfWeekHandler _wednesdayHandler = new WednesdayHandler();
        [Test]
        public void ShouldHandleNumbersDivisibleBy3And5_CanHandle_ReturnTrue()
        {
            INumeralHandler divisibleByThreeAndFive = new DivisibleByThreeAndFiveNumeralHandler(_wednesdayHandler);
            Assert.AreEqual(true, divisibleByThreeAndFive.CanHandle(15));
        }

        [Test]
        public void ShouldNotHandleNumbersNotDivisibleByThreeAndFive_CanHandle_ReturnFalse()
        {
            INumeralHandler divisibleByThreeAndFive = new DivisibleByThreeAndFiveNumeralHandler(_wednesdayHandler);
            Assert.AreEqual(false, divisibleByThreeAndFive.CanHandle(7));
        }


        [Test]
        public void IfWednesdayGetFizzBuzzElseWizzWuzz_GetNumberInStringFormat_ReturnsString()
        {
            INumeralHandler divisibleByThreeAndFive = new DivisibleByThreeAndFiveNumeralHandler(_wednesdayHandler);
            Assert.AreEqual(DateTime.Today.DayOfWeek == DayOfWeek.Wednesday ? "wizz wuzz" : "fizz buzz",
                divisibleByThreeAndFive.GetFizzBuzzWord());
        }
    }
}
