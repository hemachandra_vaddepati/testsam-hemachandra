﻿using System;
using FizzBuzz.Service.Handlers;
using NUnit.Framework;

namespace FizzBuzz.ServiceTests.Handlers
{
    [TestFixture()]
    class WednesdayHandlerTests
    {
        [Test]
        public void CanHandleWednesday_CanHandle_ReturnsTrue()
        {
            IDayOfWeekHandler dayOfWeekHandler = new WednesdayHandler();
            Assert.AreEqual(true, dayOfWeekHandler.CanHandle(DayOfWeek.Wednesday));
        }

        [Test]
        public void CanHandleMonday_CanHandle_ReturnsFalse()
        {
            IDayOfWeekHandler dayOfWeekHandler = new WednesdayHandler();
            Assert.AreEqual(false, dayOfWeekHandler.CanHandle(DayOfWeek.Monday));
        }


    }
}
