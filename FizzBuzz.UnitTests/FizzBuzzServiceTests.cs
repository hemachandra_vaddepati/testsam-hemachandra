﻿using System.Collections.Generic;
using FizzBuzz.Service;
using FizzBuzz.Service.Handlers;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace FizzBuzz.ServiceTests
{
    [TestFixture()]
    class FizzBuzzServiceTests
    {
        private readonly IDayOfWeekHandler _wednesdayHandler = new WednesdayHandler();

        [Test]
        public void GetListStringUpto294_GetFizzBuzzData_ReturnsListFromOneTo294()
        {
            List<INumeralHandler> numeralHandlers = new List<INumeralHandler>
            {
                new DivisibleByThreeNumeralHandler(_wednesdayHandler),
                new DivisibleByFiveNumeralHandler(_wednesdayHandler),
                new DivisibleByThreeAndFiveNumeralHandler(_wednesdayHandler)
            };

            FizzBuzzService fizzBuzzService = new FizzBuzzService(numeralHandlers);

            List<string> result = fizzBuzzService.GetFizzBuzzData(294);

            Assert.AreEqual(294, result.Count);
        }
    }
}
